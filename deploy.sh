#!/bin/bash
#date 2021-1-28
#auther min
#for app deploy

# stop old process
pid=`ps -ef | grep -v grep|grep 'gitpod-0.0.1' | awk -F ' ' '{print $2}'`
if [ ${pid}"x" == "x" ]; then
  echo "there is no java process."
else
    kill -9 $pid
    echo "kill pid "$pid" success!"
fi
# start new process
source /etc/profile
cd /tmp/deploy/current
nohup java -jar gitpod-0.0.1-SNAPSHOT.jar > gitpod.log &
echo "start new process success!"
exit 0

