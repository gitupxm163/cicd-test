#!/bin/bash
#date 2021-1-28
#auther min
#for mvn package

source /etc/profile
hostname
mvn_cmd=`which mvn`
$mvn_cmd clean package -Dmaven.test.skip=true
whoami
ls -a

mkdir -p /tmp/deploy

rm -rf /tmp/deploy/*.jar
cp target/*.jar /tmp/deploy/
cd /tmp/deploy

# 存储本次版本与上一次版本
if [ -d current ]; then
  rm -rf ./last
  mv current last
  mkdir current && cp *.jar ./current
else
  mkdir current
  cp *.jar ./current
fi
